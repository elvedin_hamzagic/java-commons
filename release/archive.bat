@echo off
cls
echo --------------------------------------------------------------------------
echo ProArc v1.2.0
echo --------------------------------------------------------------------------
echo;

REM ------------------------ PROJECT SPECIFIC SETTINGS ------------------------
set project=java-commons
set version=1.0.0

set drive=D:
set root=\Elvedin\Projects\Common\Source\java-commons
set release=%drive%%root%\release
set src=%drive%%root%\source\java-commons
set bin=^
%src%\java-commons\release\java-commons.jar

set excludes=-x!.* -x!log
REM ---------------------------------------------------------------------------

for /F "tokens=1,2,3,4,5 delims=- " %%i in ('date /t') do set yyyymmdd=%%i.%%j.%%k
%drive%
cd %release%

echo Archiving files ...
echo from: %src%
echo to:   %release%

echo;
if exist "%project%_%yyyymmdd%_v%version%.src.7z" (
echo File "%project%_%yyyymmdd%_v%version%.src.7z" exists.
set /p archive=Overwrite [y/n]?
) else ( set archive=y )
if %archive% == y ( 7z a -t7z -mx9 "%project%_%yyyymmdd%_v%version%.src.7z" "%src%\*" %excludes% )

echo;
if exist "%project%_%yyyymmdd%_v%version%.bin.7z" (
echo File "%project%_%yyyymmdd%_v%version%.bin.7z" exists.
set /p overwrite=Overwrite [y/n]?
) else ( set archive=y )
if %archive% == y ( 7z a -t7z -mx9 "%project%_%yyyymmdd%_v%version%.bin.7z" %bin% )

echo;
echo --------------------------------------------------------------------------
echo FINISHED!
pause
