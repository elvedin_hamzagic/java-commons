package com.codeinspace;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Settings
{
	public static class Config
	{
		public static final String DEFAULT_NAME = "config.properties";
		private Properties properties = new Properties();
		private String name = DEFAULT_NAME;
		private String path = "";
		
		public Config() {}
		public Config(String name, String path) { this.name = name; this.path = path; }
		
		public String getName() { return name; }
		public String getPath() { return path; }
		
		private String getFullPath() { return path + name; }
		
		public String get(String name)
		{
			try {
				InputStream stream = new FileInputStream(getFullPath());
				properties.load(stream);
				String value = properties.getProperty(name); stream.close();
				return value; }
			catch (Exception ex) {
				Log.warning(ex, "Failed to get property '%s'from config file.", name); }
			return null;
		}
		
		public String get(String name, Object defaultValue)
		{
			String value = get(name);
			if (value != null) return value;
			return defaultValue.toString();
		}
		
		public Boolean getBoolean(String name, Boolean defaultValue) {
			return Boolean.valueOf(get(name, defaultValue.toString())); }
		
		public Character getChar(String name, Character defaultValue) {
			return Character.valueOf(get(name, defaultValue.toString()).charAt(0)); }
		
		public Byte getByte(String name, Byte defaultValue) {
			return Byte.valueOf(get(name, defaultValue.toString())); }

		public Short getShort(String name, Short defaultValue) {
			return Short.valueOf(get(name, defaultValue.toString())); }

		public Integer getInteger(String name, Integer defaultValue) {
			return Integer.valueOf(get(name, defaultValue.toString())); }
		
		public Long getLong(String name, Long defaultValue) {
			return Long.valueOf(get(name, defaultValue.toString())); }

		public Float getFloat(String name, Float defaultValue) {
			return Float.valueOf(get(name, defaultValue.toString())); }

		public Double getDouble(String name, Double defaultValue) {
			return Double.valueOf(get(name, defaultValue.toString())); }
		
		public boolean set(String name, String value)
		{
			try {
				OutputStream stream = new FileOutputStream(getFullPath());
				properties.setProperty(name, value);
				properties.store(stream, null); stream.close();
				return true; }
			catch (Exception ex) { Log.warning(ex, "Failed to set property in config file."); }
			return false;
		}
	}
	
	private static Config config = new Config();
	public static void init(Config config) { Settings.config = config; }
	
	public static Config getConfig() { return config; }
	
	public static String get(String name) { return config.get(name); }
	
	public static String get(String name, Object defaultValue) {
		return config.get(name, defaultValue); }
	
	public static Boolean getBoolean(String name, Boolean defaultValue) {
		return config.getBoolean(name, defaultValue); }
	
	public static Character getCharacter(String name, Character defaultValue) {
		return config.getChar(name, defaultValue); }

	public Byte getByte(String name, Byte defaultValue) {
		return config.getByte(name, defaultValue); }

	public static Short getShort(String name, Short defaultValue) {
		return config.getShort(name, defaultValue); }

	public static Integer getInteger(String name, Integer defaultValue) {
		return config.getInteger(name, defaultValue); }

	public static Long getLong(String name, Long defaultValue) {
		return config.getLong(name, defaultValue); }

	public Float getFloat(String name, Float defaultValue) {
		return config.getFloat(name, defaultValue); }

	public Double getDouble(String name, Double defaultValue) {
		return config.getDouble(name, defaultValue); }
	
	public static boolean set(String name, String value) { return config.set(name, value); }
}
