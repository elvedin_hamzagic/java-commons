package com.codeinspace;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class SafeCollections
{
	public static class SafeMap<K, V>
	{
		private HashMap<K, V> map = new HashMap<K, V>();
		private ReentrantLock lock = new ReentrantLock();
		
		public boolean isEmpty()
		{
			try { lock.lock(); return map.isEmpty(); } finally { lock.unlock(); }
		}
		
		public boolean hasKey(K key)
		{
			try { lock.lock(); return map.containsKey(key); } finally { lock.unlock(); }
		}
		
		public V add(K key, V value)
		{
			try { lock.lock(); return map.put(key, value); } finally { lock.unlock(); }
		}
	
		public V get(K key)
		{
			try { lock.lock(); return map.get(key); } finally { lock.unlock(); }
		}
		
		public V remove(K key)
		{
			try { lock.lock(); return map.remove(key); } finally { lock.unlock(); }
		}
		
		public void forEach(BiConsumer<K, V> action)
		{
			try { lock.lock(); map.forEach(action); } finally { lock.unlock(); }
		}
	}

	public static class SafeQueue<T>
	{
		private ArrayDeque<T> list = new ArrayDeque<T>();
		private ReentrantLock lock = new ReentrantLock();
		private int maxSize = 0;
		
		public SafeQueue() {}
		public SafeQueue(int maxSize) { if (maxSize > 0) this.maxSize = maxSize; }
		
		public T get()
		{
			try { lock.lock(); return list.poll(); } finally { lock.unlock(); }
		}
		
		public ArrayList<T> getAll()
		{
			ArrayList<T> output = new ArrayList<T>();
			try { lock.lock(); while (!list.isEmpty()) output.add(list.poll()); }
			finally { lock.unlock(); }
			return output;
		}
		
		public T peek()
		{
			try { lock.lock(); return list.peek(); } finally { lock.unlock(); }
		}
		
		public boolean put(T value)
		{
			try { lock.lock(); return maxSize == 0 || list.size() < maxSize ? list.add(value) : false; }
			finally { lock.unlock(); }
		}
		
		public void empty()
		{
			try { lock.lock(); list.clear(); } finally { lock.unlock(); }
		}
		
		public void forEach(Consumer<T> action)
		{
			try { lock.lock(); list.forEach(action); } finally { lock.unlock(); }
		}
	}

	public static class SafeSet<T>
	{
		private HashSet<T> set = new HashSet<T>();
		private ReentrantLock lock = new ReentrantLock();
		
		public void add(T value)
		{
			try { lock.lock(); set.add(value); } finally { lock.unlock(); }
		}
		
		public boolean has(T value)
		{
			try { lock.lock(); return set.contains(value); } finally { lock.unlock(); }
		}
		
		public boolean remove(T value)
		{
			try { lock.lock(); return set.remove(value); } finally { lock.unlock(); }
		}
		
		public boolean isEmpty()
		{
			try { lock.lock(); return set.isEmpty(); } finally { lock.unlock(); }
		}
		
		public void forEach(Consumer<T> action)
		{
			try { lock.lock(); set.forEach(action); } finally { lock.unlock(); }
		}
	}
}
