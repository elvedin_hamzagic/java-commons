package com.codeinspace;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;

public class Log
{
	public enum Level
	{
		ALL(0), TRACE(1), INFO(2), WARNING(3), SEVERE(4), NONE(5);
		
		private final int value;
		private Level(int value) { this.value = value; }
		public int getValue() { return this.value; }
	}
	
	public static class Logger
	{
		private String name = "Logger";
		private Level level = Level.TRACE;
		private String format = "[%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS] %4$s at [%2$s()]: %5$s%6$s%n";
		private Boolean writeToFile = false;
		private String filePath = "logs/";
		private int fileLimit = 50000;
		private int filesCount = 100;
		private boolean shouldPrintCompactStackTrace = true;
		private java.util.logging.Logger logger = java.util.logging.Logger.getLogger(name);
		private ConsoleHandler consoleHandler = null;
		private FileHandler fileHandler = null;
		
		public Logger() { logger.setLevel(java.util.logging.Level.ALL); }
		public Logger(String name, Level level)
		{
			this.name = name; this.level = level;
			logger = java.util.logging.Logger.getLogger(name);
			logger.setLevel(java.util.logging.Level.ALL);
			setFormat(format);
			logger.setUseParentHandlers(false);
			addConsoleHandler();
		}
		public Logger(String name, Level level, String filePath, int fileLimit, int filesCount)
		{
			this(name, level);
			addFileHandler(filePath, fileLimit, filesCount);
		}
		
		public static String printStackTrace(Throwable err)
		{
			StringWriter sw = new StringWriter();
			err.printStackTrace(new PrintWriter(sw));
			return sw.toString();
		}
		
		public static String printCompactStackTrace(Throwable err)
		{
			StringBuilder result = new StringBuilder();
			StackTraceElement currentStackTraceElement = getCallingStackTraceElement();
			StackTraceElement previousStackTraceElement = null;
			
			while (err != null) {
				StackTraceElement[] stackTrace = err.getStackTrace();
				for (StackTraceElement e : stackTrace)
					if (e.getClassName().equals(currentStackTraceElement.getClassName()) &&
						e.getMethodName().equals(currentStackTraceElement.getMethodName())) {
						result.append(String.format("%s\t%s at %s.%s(%d): %s", result.length() == 0 ? "" : "\r\n",
							err.getClass().getSimpleName(), e.getClassName(), e.getMethodName(), e.getLineNumber(),
							err.getMessage()));
						break; }
					else previousStackTraceElement = e;
				currentStackTraceElement = previousStackTraceElement;
				err = err.getCause(); }
			
			return result.toString();
		}
		
		private static StackTraceElement getCallingStackTraceElement()
		{
			StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
			int i = 0; StackTraceElement element;
			do { i++; element = stackTraceElements[i]; }
			while(element.getClassName().startsWith(Log.class.getName()) && i < stackTraceElements.length - 1);
			return element;
		}
		
		private String formatMessage(Throwable err, String msg, Object... args)
		{
			return String.format("%s (Exception: %s)\r\n%s", String.format(msg, args), err.getMessage(),
				shouldPrintCompactStackTrace ? printCompactStackTrace(err) : printStackTrace(err));
		}
		
		public String getName() { return name; }
		public Level getLevel() { return level; }
		public String setFormat() { return format; }
		public Boolean getWriteToFile() { return writeToFile; }
		public String getFilePath() { return filePath; }
		public int getFileLimit() { return fileLimit; }
		public int getFilesCount() { return filesCount; }
		public boolean getShouldPrintCompactStackTrace() { return shouldPrintCompactStackTrace; }
		
		public Level setLevel(Level value) { this.level = value; return level; }
		public String setFormat(String value)
		{
			System.setProperty("java.util.logging.SimpleFormatter.format", value);
			this.format = value;
			return format;
		}
		public boolean setShouldPrintCompactStackTrace(boolean value) { return shouldPrintCompactStackTrace = value; }
		
		public void addConsoleHandler()
		{
			consoleHandler = new ConsoleHandler();
			consoleHandler.setLevel(java.util.logging.Level.ALL);
			logger.addHandler(consoleHandler);
		}
		
		public Boolean addFileHandler(String filePath, int fileLimit, int filesCount)
		{
			 this.filePath = filePath;
			 this.fileLimit = fileLimit;
			 this.filesCount = filesCount;
			
			try {
				File folder = new File(filePath); folder.mkdir();
				fileHandler = new FileHandler(
					String.format("%s%s_%%g.log", filePath, name), fileLimit, filesCount);
				logger.addHandler(fileHandler);
				writeToFile = true; }
			
			catch (IOException ex) {
				logger.severe(String.format(
					"at Logger.addFileHandler: Failed to create log file! (%s)", ex.getMessage()));
				return false; }
						
			return true;
		}
		
		public void removeConsoleHandler()
		{
			if (consoleHandler == null) return;
			logger.removeHandler(consoleHandler); consoleHandler = null;
		}
		
		public void removeFileHandler()
		{
			if (fileHandler == null) return;
			logger.removeHandler(fileHandler); fileHandler = null;
			writeToFile = false;
		}

		public void debug(String msg, Object... args)
		{
			logger.fine(String.format("DEBUG: %s", String.format(msg, args)));
		}
		
		public void entering(String msg, Object... args)
		{
			if (level.getValue() > Level.TRACE.getValue()) return;
			StackTraceElement element = getCallingStackTraceElement();
			logger.logp(java.util.logging.Level.FINER, element.getClassName(), element.getMethodName(),
				String.format(String.format("ENTRY (%s)", msg), args));
		}

		public void exiting(String msg, Object... args)
		{
			if (level.getValue() > Level.TRACE.getValue()) return;
			StackTraceElement element = getCallingStackTraceElement();
			logger.logp(java.util.logging.Level.FINER, element.getClassName(), element.getMethodName(),
				String.format(String.format("EXIT: (%s)", msg), args));
		}
		
		public void trace(String msg, Object... args)
		{
			if (level.getValue() > Level.TRACE.getValue()) return;
			StackTraceElement element = getCallingStackTraceElement();
			logger.logp(java.util.logging.Level.FINE, element.getClassName(), element.getMethodName(),
				String.format("TRACE: %s", String.format(msg, args)));
		}

		public void info(String msg)
		{
			if (level.getValue() > Level.INFO.getValue()) return;
			StackTraceElement element = getCallingStackTraceElement();
			logger.logp(java.util.logging.Level.INFO, element.getClassName(), element.getMethodName(), msg);
		}
		public void info(String msg, Object... args) { info(String.format(msg, args)); }
		public void info(Throwable err, String msg, Object... args) { info(formatMessage(err, msg, args)); }
		
		public void warning(String msg)
		{
			if (level.getValue() > Level.WARNING.getValue()) return;
			StackTraceElement element = getCallingStackTraceElement();
			logger.logp(java.util.logging.Level.WARNING, element.getClassName(), element.getMethodName(), msg);
		}
		public void warning(String msg, Object... args) { warning(String.format(msg, args)); }
		public void warning(Throwable err, String msg, Object... args) { warning(formatMessage(err, msg, args)); }
		public void warning(Throwable err) { warning(err.getMessage()); }

		public void severe(String msg)
		{
			if (level.getValue() > Level.SEVERE.getValue()) return;
			StackTraceElement element = getCallingStackTraceElement();
			logger.logp(java.util.logging.Level.SEVERE, element.getClassName(), element.getMethodName(), msg);
		}
		public void severe(String msg, Object... args) { severe(String.format(msg, args)); }
		public void severe(Throwable err, String msg, Object... args) { severe(formatMessage(err, msg, args)); }
		public void severe(Throwable err) { severe(err.getMessage()); }
	}
	
	private static Logger logger = new Logger();
	public static void init(Logger logger) { Log.logger = logger; }
	
	public static Logger getLogger() { return logger; }
	
	public static void debug(String msg, Object... args) { logger.debug(msg, args); }
	public static void entering(String msg, Object... args) { logger.entering(msg, args); }
	public static void exiting(String msg, Object... args) { logger.exiting(msg, args); }
	
	public static void trace(String msg, Object... args) { logger.trace(msg, args); }

	public static void info(String msg) { logger.info(msg); }
	public static void info(String msg, Object... args) { logger.info(msg, args); }
	public static void info(Throwable err, String msg, Object... args) { logger.info(err, msg, args); }
	
	public static void warning(String msg) { logger.warning(msg); }
	public static void warning(String msg, Object... args) { logger.warning(msg, args); }
	public static void warning(Throwable err, String msg, Object... args) { logger.warning(err, msg, args); }
	public static void warning(Throwable err) { logger.warning(err); }

	public static void severe(String msg) { logger.severe(msg); }
	public static void severe(String msg, Object... args) { logger.severe(msg, args); }
	public static void severe(Throwable err, String msg, Object... args) { logger.severe(err, msg, args); }
	public static void severe(Throwable err) { logger.severe(err); }
	
	public static String printStackTrace(Throwable err) { return Logger.printStackTrace(err); }
	public static String printCompactStackTrace(Throwable err) { return Logger.printCompactStackTrace(err); }
	
	public static String print(Map<String, Object> value)
	{
		String result = "";
		for (Map.Entry<String, Object> entry : value.entrySet())
			result += String.format("%s%s: %s", result.length() == 0 ? "" : ", ",
				Log.print(entry.getKey()), Log.print(entry.getValue()));
		return String.format("{ %s }", result);
	}
	
	public static String print(Object[] value)
	{
		String result = "";
		for (Object entry : value)
			result += String.format("%s%s", result.length() == 0 ? "" : ", ", Log.print(entry));
		return String.format("[ %s ]", result);
	}
	
	public static String print(Object value)
	{
		return String.format("<%s>", value.toString());
	}
}
